﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.BodyIndexBasics
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using System.Drawing;
    using System.Windows.Forms;
    




    /// <summary>
    /// Interaction logic for the MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private const int BytesPerPixel = 4;

        /// <summary>
        /// Collection of colors to be used to display the BodyIndexFrame data.
        /// </summary>
        private static readonly uint[] BodyColor =
        {
            0x00FFB997,
            0x00F67E7D,
            0x00843B62,
            0x000B032D,
            0x0074546A,
            0x00982649,
        };

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for body index frames
        /// </summary>
        private BodyIndexFrameReader bodyIndexFrameReader = null;

        /// <summary>
        /// Description of the data contained in the body index frame
        /// </summary>
        private FrameDescription bodyIndexFrameDescription = null;

        /// <summary>
        /// Bitmap to display
        /// </summary>
        private WriteableBitmap bodyIndexBitmap = null;

        //Implementaca obrazka
        //"C:/Users/Anna/Desktop/Kinect/2/BodyIndexBasics-WPF/Images/proba.bmp"
        private BitmapImage source1 = new BitmapImage(new Uri("C:/Users/Anna/Desktop/Kinect/2/BodyIndexBasics-WPF/Images/czlo.bmp", UriKind.Absolute));
        private BitmapImage[] source1 = new BitmapImage[4];
        //BitmapImage[1] = newnew Uri("C:/Users/Anna/Desktop/Kinect/2/BodyIndexBasics-WPF/Images/czlo.bmp", UriKind.Absolute));
        
        private WriteableBitmap obrazek2 = null; //new WriteableBitmap(source1);


        private System.Windows.Forms.Timer timer1;

        /// <summary>
        /// Intermediate storage for frame data converted to color
        /// </summary>
        private uint[] bodyIndexPixels = null;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;
        private int counter = 15;

        private int punkty = 0;

        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // get the kinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();

            // open the reader for the depth frames
            this.bodyIndexFrameReader = this.kinectSensor.BodyIndexFrameSource.OpenReader();

            // wire handler for frame arrival
            this.bodyIndexFrameReader.FrameArrived += this.Reader_FrameArrived;

            this.bodyIndexFrameDescription = this.kinectSensor.BodyIndexFrameSource.FrameDescription;

            // allocate space to put the pixels being converted
            this.bodyIndexPixels = new uint[this.bodyIndexFrameDescription.Width * this.bodyIndexFrameDescription.Height];

            // create the bitmap to display
            this.bodyIndexBitmap = new WriteableBitmap(this.bodyIndexFrameDescription.Width, this.bodyIndexFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);

            this.obrazek2 = new WriteableBitmap(source1);

            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // use the window object as the view model in this simple example
            this.DataContext = this;

             

                        // initialize the components (controls) of the window

            this.InitializeComponent();

            this.timer1 = new Timer();
            this.timer1.Tick += new EventHandler(timer1_Tick);
            this.timer1.Interval = 1000; // 1 second
            counter = 6;
            this.timer1.Start();

            //this.Timer.Tick += new EventHandler(Timer_Click);

            // //this.Timer.Interval = new TimeSpan(0, 0, 1);
            // this.Timer.Interval = 1000;
            //this.Timer.Start();

        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                //this.obrazek = new writab("C:/Users/Anna/Desktop/Kinect/2/BodyIndexBasics-WPF/Images/proba.bmp", true);

                //return this.obrazek2;
                return this.bodyIndexBitmap;
            }
        }
        private void Timer_Click(object sender, EventArgs e)

       {

            //DateTime d;

            //d = DateTime.Now;

            //label1.Content = 60 - d.Second;

            //this.Timer.Tick += new EventHandler(Timer_Click);

            // //this.Timer.Interval = new TimeSpan(0, 0, 1);
            // this.Timer.Interval = 1000;
            //this.Timer.Start();


            //counter = 15;
           // timer1.Start();
            

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;

            if (counter == -1)
            {
                timer1.Stop();
                counter = 15;
            }
                //for(counter = 15; counter >=0; counter--)
            //{
            //    if (counter == 0)
            //        timer1.Stop();
            //}
            label1.Content = counter.ToString();
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public int  Punkty
        {
            get
            {
                return this.punkty;
            
                    }
             
        }
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        /// 
      

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.bodyIndexFrameReader != null)
            {
                // remove the event handler
                this.bodyIndexFrameReader.FrameArrived -= this.Reader_FrameArrived;

                // BodyIndexFrameReder is IDisposable
                this.bodyIndexFrameReader.Dispose();
                this.bodyIndexFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the body index frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, BodyIndexFrameArrivedEventArgs e)
        {
            bool bodyIndexFrameProcessed = false;

            using (BodyIndexFrame bodyIndexFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyIndexFrame != null)
                {
                    // the fastest way to process the body index data is to directly access 
                    // the underlying buffer
                    using (Microsoft.Kinect.KinectBuffer bodyIndexBuffer = bodyIndexFrame.LockImageBuffer())
                    {
                        // verify data and write the color data to the display bitmap
                        if (((this.bodyIndexFrameDescription.Width * this.bodyIndexFrameDescription.Height) == bodyIndexBuffer.Size) &&
                            (this.bodyIndexFrameDescription.Width == this.bodyIndexBitmap.PixelWidth) && (this.bodyIndexFrameDescription.Height == this.bodyIndexBitmap.PixelHeight))
                        {
                            this.ProcessBodyIndexFrameData(bodyIndexBuffer.UnderlyingBuffer, bodyIndexBuffer.Size);
                            bodyIndexFrameProcessed = true;
                        }
                    }
                }
            }

            if (bodyIndexFrameProcessed)
            {
                this.RenderBodyIndexPixels();
            }
        }

        /// <summary>
        /// Directly accesses the underlying image buffer of the BodyIndexFrame to 
        /// create a displayable bitmap.
        /// This function requires the /unsafe compiler option as we make use of direct
        /// access to the native memory pointed to by the bodyIndexFrameData pointer.
        /// </summary>
        /// <param name="bodyIndexFrameData">Pointer to the BodyIndexFrame image data</param>
        /// <param name="bodyIndexFrameDataSize">Size of the BodyIndexFrame image data</param>
        private unsafe void ProcessBodyIndexFrameData(IntPtr bodyIndexFrameData, uint bodyIndexFrameDataSize)
        {
            byte* frameData = (byte*)bodyIndexFrameData;

            //int stride = obrazek2.PixelWidth * 4;
            //int size = obrazek2.PixelHeight * stride;
            //byte[] pixelData = new byte[size];
            //obrazek2.CopyPixels(pixelData, stride, 0);
           
             int h = obrazek2.PixelHeight;
             int w = obrazek2.PixelWidth;
             int[] pixelData = new int[h * w];
            int wightInByte = 4 * w;
            obrazek2.CopyPixels(pixelData, wightInByte, 0);
            

            // convert body index to a visual representation
            for (int i = 0; i < (int)bodyIndexFrameDataSize; ++i)
            {
                
              

             
                // the BodyColor array has been sized to match
                // BodyFrameSource.BodyCount
                if (frameData[i] < BodyColor.Length)
                {
                    // this pixel is part of a player,
                    // display the appropriate color
                    if (pixelData[i] == 0x00000000)
                    {
                        this.bodyIndexPixels[i] = 0xFFFF4000;
                        

                    }
                    else

                    {
                        this.bodyIndexPixels[i] = BodyColor[frameData[i]];
                        punkty = punkty + 1;
                        this.Punkty.Equals(punkty);
                    }

                }
                else
                {
                    // this pixel is not part of a player
                    // display black
                    if (pixelData[i] == 0x00000000)
                    {
                        this.bodyIndexPixels[i] = 0x00000000;
                    }
                    else
                        this.bodyIndexPixels[i] = 0xFFFFFFFF;

                }
            }
        }

        /// <summary>
        /// Renders color pixels into the writeableBitmap.
        /// </summary>
        private void RenderBodyIndexPixels()
        {
            this.bodyIndexBitmap.WritePixels(
                new Int32Rect(0, 0, this.bodyIndexBitmap.PixelWidth, this.bodyIndexBitmap.PixelHeight),
                this.bodyIndexPixels,
                this.bodyIndexBitmap.PixelWidth * (int)BytesPerPixel,
                0);
            

        }

        

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }
    }

   
}
